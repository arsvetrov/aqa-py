# task 01 == Розділіть змінну так, щоб вона займала декілька фізичних лінії
# task 02 == Знайдіть та екрануйте всі символи одинарної дужки у тексті
# task 03 == Виведіть змінну alice_in_wonderland на друк


# Solution task 01,02,03

alice_in_wonderland = ' - Would you tell me, please, which way I ought to go from here?'\
    '\n"That depends a good deal on where you want to get to." - said the Cat.'\
    '\n"I don\'t much care where." - said Alice.'\
    '\n"Then it doesn\'t matter which way you go." - said the Cat.'\
    '\n"So long as I get somewhere." -  Alice added as an explanation.'\
    '\n"Oh, you\'re sure to do that." - said the Cat.'\
    '\n - If you only walk long enough.'

# print(alice_in_wonderland)

"""
# Задачі 04 -10:
# Переведіть задачі з книги "Математика, 5 клас"
# на мову пітон і виведіть відповідь, так, щоб було
# зрозуміло дитині, що навчається в п'ятому класі
"""
# task 04
"""
Площа Чорного моря становить 436 402 км2, а площа Азовського
моря становить 37 800 км2. Яку площу займають Чорне та Азов-
ське моря разом?
"""
# task 04 Solution


def sea_square_sum(sea1_name: str, sea2_name: str, sea1_value: int, sea2_value: int) -> int:
    sum_result = sea1_value + sea2_value
    print(f'The sum square of {sea1_name} and {sea2_name} seas equal: {sum_result}')
    return sum_result

# User can add another two seas str names and int values for calculating square sum for another seas.
# sea_square_sum(sea1_name='Black', sea2_name='Azov', sea1_value=436402, sea2_value=37800)


# task 05
"""
Мережа супермаркетів має 3 склади, де всього розміщено
375 291 товар. На першому та другому складах перебуває
250 449 товарів. На другому та третьому – 222 950 товарів.
Знайдіть кількість товарів, що розміщені на кожному складі.
"""
# task 05 Solution


def define_stock_goods_count(goods_all: int, first_and_second: int, second_and_third: int) -> dict:
    """Receives args all count of goods, sum of goods in first and second stocks,
    Receives args all count of goods, sum of goods in second  and third  stocks then calculates count of
    goods each stock and returns dict with name of stock and correspond count of goods"""
    stock_1 = goods_all - second_and_third
    stock_2 = first_and_second - stock_1
    stock_3 = goods_all - (stock_1 + stock_2)
    print(f'Goods in First stock: {stock_1} \nGoods in Second stock: {stock_2} \nGoods in Third stock: {stock_3}')

    return {
           "first_stock": stock_1,
           "second_stock": stock_2,
           "third_stock": stock_3
    }


# task 06
"""
Михайло разом з батьками вирішили купити комп’ютер, ско-
риставшись послугою «Оплата частинами». Відомо, що сплачу-
вати необхідно буде півтора року по 1179 грн/місяць. Обчисліть
вартість комп’ютера.
"""
# task 06 Solution


def desktop_price_define(count_month: int, month_payment: int) -> int:
    """
    Receives two positive numbers count month and month payment then
    calculates credit sum and returns result. In case when numbers not positive
    or 0 prints to user correspond message and returns None
     """
    if count_month <= 0:
        print('Count month cant\'t be 0 or less')
    elif month_payment <= 0:
        print('Month payment cant\'t be 0 or less')
    else:
        total_price = month_payment * count_month
        print(f'Total desktop price equal: {total_price} grn.')
        return total_price


#  desktop_price_define(count_month=0, month_payment=1179)


# task 07
"""
Знайди остачу від діленя чисел:
a) 8019 : 8     d) 7248 : 6
b) 9907 : 9     e) 7128 : 5
c) 2789 : 5     f) 19224 : 9
"""

# task 07 Solution

choices = {
    "a) 8019 : 8 ": (8019, 8),
    "b) 9907 : 9": (9907, 9),
    "c) 2789 : 5": (2789, 5),
    "d) 7248 : 6": (7248, 6),
    "e) 7128 : 5": (7128, 5),
    "f) 19224 : 9": (19224, 9)

}

# Processing args dict and return dict where keys are task input values and values are division reminders


def define_division_reminder(args: dict) -> dict:
    result_dict = {}
    for keys, values in args.items():
        result = (values[0] / values[1]) % 1
        result_dict[keys] = result
        print(f'Division remainder result: \n"{keys}" = {result}')  # It's planned output
    return result_dict


# define_division_reminder(choices)


# task 08
"""
Іринка, готуючись до свого дня народження, склала список того,
що їй потрібно замовити. Обчисліть, скільки грошей знадобиться
для даного її замовлення.
Назва товару    Кількість   Ціна
Піца велика     4           274 грн
Піца середня    2           218 грн
Сік             4           35 грн
Торт            1           350 грн
Вода            3           21 грн
"""

# task 08 Solution

goods_list = {
    "pizza_L": (274, 4),
    "pizza_M": (218, 2),
    "Juice": (35, 4),
    "Cake": (350, 1),
    "Water": (21, 3)
}


def money_calculate(args: dict) -> int:
    result = 0
    for goods, prices in args.items():
        result += prices[0] * prices[1]
    print(f'Irinka has to steal {result} grn for her hangover')
    return result


# money_calculate(goods_list)


# task 09
"""
Ігор займається фотографією. Він вирішив зібрати всі свої 232
фотографії та вклеїти в альбом. На одній сторінці може бути
розміщено щонайбільше 8 фото. Скільки сторінок знадобиться
Ігорю, щоб вклеїти всі фото?
"""
# task 09 Solution


def solve_igors_math_troubles(photos_all: int, max_photos_on_page: int) -> int:
    """Receives count of photos number > 0 and max count photos on page number > 0
    returns result how much pages need for all photos."""
    if photos_all % max_photos_on_page != 0:
        result = (photos_all // max_photos_on_page) + 1
        print(f'Igor needs {result} pages in his album')
        return result
    else:
        result = photos_all // max_photos_on_page
        print(f'Igor needs {result} pages in his album')
        return result


#  solve_igors_math_troubles(photos_all=232, max_photos_on_page=8)

"""
# Code wars style

solve_math_troubles = (lambda photos_all, max_photos_on_page: (photos_all // max_photos_on_page) + 1
    if photos_all % max_photos_on_page != 0 else photos_all // max_photos_on_page)
print(solve_math_troubles(photos_all=232, max_photos_on_page=8))
"""


# task 10
"""
Родина зібралася в автомобільну подорож із Харкова в Буда-
пешт. Відстань між цими містами становить 1600 км. Відомо,
що на кожні 100 км необхідно 9 літрів бензину. Місткість баку
становить 48 літрів.
1) Скільки літрів бензину знадобиться для такої подорожі?
2) Скільки щонайменше разів родині необхідно заїхати на зап-
равку під час цієї подорожі, кожного разу заправляючи пов-
ний бак?
"""


def calculate_fuel_for_trip(distance: int, tank_volume: int = 48, efforts: int = 9) -> list:
    """Receives required number arg distance and optional number tank volume, number efforts
     returns list with float and int counts where first number is count of fuels needed and second number
     count of attends gas station"""
    fuel_for_trip = (distance / 100) * efforts
    attends_gas_station = fuel_for_trip // tank_volume
    if attends_gas_station % 1 > 0:
        attends_gas_station += 1

    print(f'Family need {fuel_for_trip} litres fuel for trip and attend gas station {int(attends_gas_station)} times.')
    return [fuel_for_trip, int(attends_gas_station)]


#  calculate_fuel_for_trip(distance=1600)
