small_list = [3, 1, 4, 5, 2, 5, 3]
big_list = [3, 5, -2, -1, -3, 0, 1, 4, 5, 2]
# task 1. Знайдіть всі унікальні елементи в списку small_list

# task 1 Solution

print(set(small_list))


# task 2. Знайдіть середнє арифметичне всіх елементів у списку small_list

# task 2 Solution

print(sum(small_list) / len(small_list))

# task 3. Перевірте, чи є в списку big_list дублікати

# task 3 Solution

print(len(set(big_list)) != len(big_list))


base_dict = {'contry': 'Ukraine', 'continent': 'Europe', 'size': 123}
add_dict = {"a": 1, "b": 2, "c": 2, "d": 3, 'size': 12}
# task 4. Знайдіть ключ з максимальним значенням у словнику add_dict

# task 4 Solution

print(max(add_dict.keys()))


# task 5. Створіть новий словник, в якому ключі та значення будуть
# замінені місцями у заданому словнику

# task 5 Solution

new_dict = {}
for keys, values in add_dict.items():
    new_dict[values] = keys

print(new_dict)

# task 6. Об'єднайте два словника base_dict та add_dict  в новий словник sum_dict
# Якщо ключі збігаються, то об'єднайте (str) або додайте їх значення (int)
# sum_dict = {}

# task 6 Solution -- Described with coach checking instance is not necessary

sum_dict = base_dict.copy()
for key, value in add_dict.items():
    if key in sum_dict:
        sum_dict[key] += value
    else:
        sum_dict[key] = value

print(sum_dict)

# task 7.
line = "Створіть множину всіх символів, які входять у заданий рядок"

# task 7 Solution

sample_set = set(line)
for i in line:
    sample_set.add(i)

print(sample_set)

# task 8. Обчисліть суму елементів двох множин, які не є спільними

# task 8 Solution

set_1 = {1, 2, 3, 4, 5}
set_2 = {4, 6, 5, 10}
print(sum(set_1.symmetric_difference(set_2)))


# task 9. Створіть два списки та обробіть їх так, щоб отримати сет, який
# містить всі елементи з обох списків,  які зустрічаються тільки один раз.
# Наприклад, якщо перший список містить [1, 2, 3, 4], а другий
# список містить [3, 4, 5, 6], то повернутий сет містить [1, 2, 5, 6]

# task 9 Solution # Output is set described with coach

lst1 = [1, 2, 3, 4]
lst2 = [3, 4, 5, 6]

print(set(lst1).symmetric_difference(lst2))


person_list = [('Alice', 25), ('Boby', 19), ('Charlie', 32),
               ('David', 28), ('Emma', 22), ('Frank', 45)]
# task 10. Обробіть список кортежів person_list, що містять ім'я та вік людей,
# так, щоб отримати словник, де ключі - вікові діапазони (10-19, 20-29 тощо),
# а значення - списки імен людей, які потрапляють в кожен діапазон.
# Приклад виводу:
# {'10-19': ['A'], '20-29': ['B', 'C', 'D'], '30-39': ['E'], '40-49': ['F']}

# task 10 Solution

groped_result = {'10-19': [], '20-29': [], '30-39': [], '40-49': []}
for data in person_list:

    if data[1] in range(10, 20):
        groped_result['10-19'].append(data[0])
    elif data[1] in range(20, 30):
        groped_result['20-29'].append(data[0])
    elif data[1] in range(30, 40):
        groped_result['30-39'].append(data[0])
    elif data[1] in range(40, 50):
        groped_result['40-49'].append(data[0])

print(groped_result)
