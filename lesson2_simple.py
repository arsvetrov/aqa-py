# task 01 == Розділіть змінну так, щоб вона займала декілька фізичних лінії
# task 02 == Знайдіть та екрануйте всі символи одинарної дужки у тексті
# task 03 == Виведіть змінну alice_in_wonderland на друк

# Solution task 01,02,03

alice_in_wonderland = ' - Would you tell me, please, which way I ought to go from here?'\
    '\n"That depends a good deal on where you want to get to." - said the Cat.'\
    '\n"I don\'t much care where." - said Alice.'\
    '\n"Then it doesn\'t matter which way you go." - said the Cat.'\
    '\n"So long as I get somewhere." -  Alice added as an explanation.'\
    '\n"Oh, you\'re sure to do that." - said the Cat.'\
    '\n - If you only walk long enough.'

# print(alice_in_wonderland)

"""
# Задачі 04 -10:
# Переведіть задачі з книги "Математика, 5 клас"
# на мову пітон і виведіть відповідь, так, щоб було
# зрозуміло дитині, що навчається в п'ятому класі
"""
# task 04
"""
Площа Чорного моря становить 436 402 км2, а площа Азовського
моря становить 37 800 км2. Яку площу займають Чорне та Азов-
ське моря разом?
"""
# task 04 Solution

sea1_name = 'Black'
sea2_name = 'Azov'
sea1_value = 436402
sea2_value = 37800

sum_result = sea1_value + sea2_value
print(f'The sum squares of {sea1_name} and {sea2_name} seas equal: {sum_result}')


# task 05
"""
Мережа супермаркетів має 3 склади, де всього розміщено
375 291 товар. На першому та другому складах перебуває
250 449 товарів. На другому та третьому – 222 950 товарів.
Знайдіть кількість товарів, що розміщені на кожному складі.
"""
# task 05 Solution

goods_all = 375291
goods_first_and_second = 250449
goods_second_and_third = 222950

stock_1 = goods_all - goods_second_and_third
stock_2 = goods_first_and_second - stock_1
stock_3 = goods_all - (stock_1 + stock_2)
print(f'Goods in First stock: {stock_1} \nGoods in Second stock: {stock_2} \nGoods in Third stock: {stock_3}')


# task 06
"""
Михайло разом з батьками вирішили купити комп’ютер, ско-
риставшись послугою «Оплата частинами». Відомо, що сплачу-
вати необхідно буде півтора року по 1179 грн/місяць. Обчисліть
вартість комп’ютера.
"""
# task 06 Solution

count_month = 18
month_payment = 1179

total_price = month_payment * count_month
print(f'Total desktop price equal: {total_price} grn.')


# task 07
"""
Знайди остачу від діленя чисел:
a) 8019 : 8     d) 7248 : 6
b) 9907 : 9     e) 7128 : 5
c) 2789 : 5     f) 19224 : 9
"""

# task 07 Solution

choices = [
     ("a) 8019 : 8 ", 8019, 8),
     ("b) 9907 : 9", 9907, 9),
     ("2789 : 5", 2789, 5),
     ("d) 7248 : 6", 7248, 6),
     ("e) 7128 : 5", 7128, 5),
     ("f) 19224 : 9", 19224, 9)
]

for values in choices:
    result = (values[1] / values[2]) % 1
    name = values[0]
    print(f'Division remainder result: \n"{name}" = {result}')  # It's planned output

# task 08
"""
Іринка, готуючись до свого дня народження, склала список того,
що їй потрібно замовити. Обчисліть, скільки грошей знадобиться
для даного її замовлення.
Назва товару    Кількість   Ціна
Піца велика     4           274 грн
Піца середня    2           218 грн
Сік             4           35 грн
Торт            1           350 грн
Вода            3           21 грн
"""

# task 08 Solution

goods_list = [
    (274, 4),
    (218, 2),
    (35, 4),
    (350, 1),
    (21, 3)
]

result = 0
for values in goods_list:
    result += values[0] * values[1]
print(f'Irinka has to steal {result} grn for her hangover')


# task 09
"""
Ігор займається фотографією. Він вирішив зібрати всі свої 232
фотографії та вклеїти в альбом. На одній сторінці може бути
розміщено щонайбільше 8 фото. Скільки сторінок знадобиться
Ігорю, щоб вклеїти всі фото?
"""
# task 09 Solution
photos_all = 232
max_photos_on_page = 8

print(f'Igor needs {photos_all // max_photos_on_page} pages in his album')


# task 10
"""
Родина зібралася в автомобільну подорож із Харкова в Буда-
пешт. Відстань між цими містами становить 1600 км. Відомо,
що на кожні 100 км необхідно 9 літрів бензину. Місткість баку
становить 48 літрів.
1) Скільки літрів бензину знадобиться для такої подорожі?
2) Скільки щонайменше разів родині необхідно заїхати на зап-
равку під час цієї подорожі, кожного разу заправляючи пов-
ний бак?
"""

distance = 1600
efforts = 9
tank_volume = 48

fuel_for_trip = (distance // 100) * efforts
attends_gas_station = fuel_for_trip // tank_volume
print(f'Family need {fuel_for_trip} litres fuel for trip and attend gas station {attends_gas_station} times.')

