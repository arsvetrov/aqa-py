"""
task 01 == Виправте синтаксичні помилки
"""
# print('Hello', end=" ")
# print('world!')

"""
task 02 == Виправте синтаксичні помилки
"""

# hello = "Hello"
# world = "world"
# if True:
#     print(f"{hello} {world}!")

"""
task 03  == Вcтавте пропущену змінну у ф-цію print
"""

# for letter in "Hello world!":
#     print(letter)

"""
task 04 == Зробіть так, щоб кількість бананів була
# завжди в чотири рази більша, ніж яблук
apples = 2
banana = x
"""
# apples = 2
# banana = apples * 4

"""
task 05 == виправте назви змінних
1_storona = 1
$torona_2 = 2
сторона_3 = 3
__orona_4 = 4
"""

# side_1 = 1
# side_2 = 2
# side_3 = 3
# side_4 = 4

"""
task 06 == Порахуйте периметр фігури з task 05
та виведіть його для користувача
perimetery = x + x + x + x
print()
"""


# perimetery = side_1 + side_2 + side_3 + side_4
# print(perimetery)

# Another solution


def get_args() -> tuple:

    print('>>> Enter the first value')
    side_1 = num_validate()

    print('>>> Enter the second value')
    side_2 = num_validate()

    print('>>> Enter the third value')
    side_3 = num_validate()

    print('>>> Enter the fourth value')
    side_4 = num_validate()

    args = side_1, side_2, side_3, side_4
    return args


def num_validate() -> float:
    while True:
        input_arg = input('>> ')
        try:
            arg = float((input_arg.replace(',', '.')))
            if arg > 0:
                return arg
            else:
                print('>>> Side can\'t be negative or \'0\' Try again!')
                continue
        except ValueError:
            print('>>> Please enter a number values. Try again!')
            continue


def return_perimeter(args: tuple) -> float:
    print(f'>>> Your perimeter is: {sum(args)}')
    return sum(args)


return_perimeter(get_args())


"""
task 07
У саду посадили 4 яблуні. Груш на 5 більше яблунь, а слив - на 2 менше.
Скільки всього дерев посадили в саду?
"""


def tree_count(apple_trees=4) -> int:
    pear_trees = apple_trees + 5
    plum_trees = apple_trees - 2
    tree_count_result = apple_trees + pear_trees + plum_trees
    print(f'Count of trees in garden: {tree_count_result}')
    return tree_count_result


# tree_count()

"""
task 08
До обіда температура повітря була на 5 градусів вище нуля.
Після обіду температура опустилася на 10 градусів.
Надвечір потепліло на 4 градуси. Яка температура надвечір?
"""


def define_evening_temperature(before_noon_temperature=5) -> int:
    afternoon_temperature = before_noon_temperature - 10
    evening_temperature = afternoon_temperature + 4
    print(f'Temperature in evening is: {evening_temperature}')
    return evening_temperature


# define_evening_temperature()


"""
task 09
Взагалі у театральному гуртку - 24 хлопчики, а дівчаток - вдвічі менше.
1 хлопчик захворів та 2 дівчинки не прийшли сьогодні.
Скількі сьогодні дітей у театральному гуртку?
"""


def children_count(boys_count=24) -> int:
    girls_count = boys_count // 2
    boys_present = boys_count - 1
    girls_present = girls_count - 2
    children_result = boys_present + girls_present
    print(f'>>> Children count today is: {children_result}!')
    return children_result


# children_count()

"""
task 10
Перша книжка коштує 8 грн., друга - на 2 грн. дороже,
а третя - як половина вартості першої та другої разом.
Скільки будуть коштувати усі книги, якщо купити по одному примірнику?
"""


def book_price(first_book=8) -> float:
    second_book = first_book + 2
    third_book = (first_book + second_book) / 2
    book_price_result = first_book + second_book + third_book
    print(f'>>> Price of books: {book_price_result}')
    return book_price_result

# book_price()
