from typing import Union


# task 1
""" Задача - надрукувати табличку множення на задане число, але
лише до максимального значення для добутку - 25.
Код майже готовий, треба знайти помилки та випраавити доповнити.
"""


def multiplication_table(number: int):
    """Prints multiplication table based on given number.
    Maximum returned result is 25 """
    multiplier = 1

    while multiplier:
        result = number * multiplier
        if result > 25:
            break
        print(str(number) + "x" + str(multiplier) + "=" + str(result))
        multiplier += 1


multiplication_table(3)
# Should print:
# 3x1=3
# 3x2=6
# 3x3=9
# 3x4=12
# 3x5=15


# task 2
"""  Написати функцію, яка обчислює суму двох чисел.
"""


def two_numbers_sum(operand_1: Union[int, float], operand_2: Union[int, float]) -> Union[int, float]:
    """ Calculates sum of two given operands"""
    return operand_1 + operand_2


# task 3
"""  Написати функцію, яка розрахує середнє арифметичне списку чисел.
"""


def numbers_list_average(numbers_list: list) -> float:
    """ Calculates arithmetic mean from list with numbers"""
    return sum(numbers_list) / len(numbers_list)


# task 4
"""  Написати функцію, яка приймає рядок та повертає його у зворотному порядку.
"""


def reversed_string(text: str) -> str:
    """Returns a reversed string from given string"""
    return text[::-1]


# task 5
"""  Написати функцію, яка приймає список слів та повертає найдовше слово у списку.
"""


def max_word(words_list: list) -> str:
    """Receives list with characters string and return max length word in words_list"""
    return max(words_list)


# task 6
"""  Написати функцію, яка приймає два рядки та повертає індекс першого входження другого рядка
у перший рядок, якщо другий рядок є підрядком першого рядка, та -1, якщо другий рядок
не є підрядком першого рядка."""


def find_substring(text1: str, text2: str) -> int:
    """
    Finds the lowest index in text if subtext found else returns -1 """
    return text1.find(text2)


str1 = "Hello, world!"
str2 = "world"
print(find_substring(str1, str2))  # поверне 7

str1 = "The quick brown fox jumps over the lazy dog"
str2 = "cat"
print(find_substring(str1, str2))  # поверне -1

# task 7
# task 8
# task 9
# task 10
"""  Оберіть будь-які 4 таски з попередніх домашніх робіт та
перетворіть їх у 4 функції, що отримують значення та повертають результат.
Обоязково документуйте функції та дайте зрозумілі імена змінним.
"""


# task 7 Solution
"""  Задано список чисел numbers, потрібно знайти список квадратів
парних чисел зі списку. Спробуйте використати if та цикл for в один рядок.
"""


def squared_numbers(numbers_list: list) -> list:
    """Receives list with numbers and square even in list"""
    result = [number ** 2 for number in numbers_list if number % 2 == 0]
    return result


# task 8 Solution
"""  Задача з використанням циклу for та continue. Задано список фруктів 'fruits'
потрібно вивести на екран всі елементи списку, окрім "orange".
fruits = ["apple", "banana", "orange", "grape", "mango"]
"""


def filter_list(args_list: list, value: str):
    """Receives args list with string values and value which need to filter from list.
     Prints result on screen"""
    for values in args_list:
        if values == value:
            continue
        else:
            print(values)


# task 9 Solution
""" Створіть новий словник, в якому ключі та значення будуть
замінені місцями у заданому словнику
"""


def keys_to_values_changing(args_dict: dict) -> dict:
    """Receives dict with values and return dict where values become keys and keys become values"""
    modified_dict = {}
    for keys, values in args_dict.items():
        modified_dict[values] = keys
    return modified_dict


# task 10 Solution
"""Знайдіть ключ з максимальним значенням у словнику"""


def dict_max_value(args_dict: dict):
    """Finds max value in dict and return it's key"""
    return max(args_dict.keys())